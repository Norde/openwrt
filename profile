#!/bin/sh

# Norde (2021)

### Output
# Looking for update
opkgInstalled="$(opkg list-installed 2> /dev/null | wc -l)"
opkgUpgradable="$(opkg list-upgradable 2> /dev/null | wc -l)"

# ausgabe updates
if [ "$opkgUpgradable" = "0" ]
  then echo "$opkgInstalled packet(s) installed." && echo
  else echo "$opkgInstalled packet(s) installed." && echo "$opkgUpgradable packet(s) updated." && echo
fi

# befehle
echo "Update commands:"
echo "List of available updates: ol (opkg list)"
echo "Update package individually: u <PACKAGE_NAME> (updates)"
echo "Update of all available packages: ou (opkg-list-updates)" && echo

echo "Backup / Restore Commands:"
echo "Save all installed package names under '/etc/config/packages.list': ob (opkg-list-backup)"
echo "Reinstall all previously saved package names: obr (opkg-list-backup-reinstall)" && echo

echo "Useful abbreviation tips: alias" && echo


### Useful aliases
alias df="df -h"
# atop update 3 sec
alias atop="atop 3"
# bmon Show all interfaces
alias bmon="bmon -p eth1,br-lan,wlan0,wlan1 -o curses"
# ol list-upgrade
alias ol="opkg list-upgradable"
# ou updates all
alias ou="opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade"
# u Updates
alias u="opkg upgrade"
# ob Save the names of the installed packages in a file
alias ob="opkg list-installed | cut -f 1 -d ' ' > /etc/config/packages.list"
# obr restore the installed packages e.g. sysupgrade
alias obr='opkg install $(cat /etc/config/packages.list)'
